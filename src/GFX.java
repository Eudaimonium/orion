import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.Random;

public class GFX extends JPanel
{
	
	public static int x = 10;
	public static DisplayMode MODE = new DisplayMode(1280,800,32,0);
	public static int maxX = 1280, maxY = 800;
	
	public static BufferedImage smallTex, smallFarTex, largeTex;
	
	public static Particles small[], medium[], large[], star[],
							smallFar[], mediumFar[], largeFar[], starFar[];
	
	
	
	public Graphics2D graphics;
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		graphics = (Graphics2D) g;
		Timers.Start();
		Update();
		Render(g);
		Timers.End();
	}
	
	public static void Initialize()
	{
		try {
			
			String path = "D:\\Code\\OrionApp\\src\\";
			
			smallTex = ImageIO.read(new File(path + "SmallParticle.png"));
			smallFarTex = ImageIO.read(new File(path + "SmallParticleFar.png"));
			largeTex = ImageIO.read(new File(path + "BigParticle.png"));
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
	
		Random rnd = new Random();
		
		small = new  Particles[15]; 	//done update + render
		medium = new  Particles[15];
		large = new  Particles[10]; 	//done update + render
		star = new  Particles[20];
		smallFar = new  Particles[15];	//done update + render
		mediumFar = new  Particles[15];
		largeFar = new  Particles[5];
		starFar = new  Particles[20];
		
		int counter = 0;
		for (int x = 0; x < 5; x++) 
		{
			for (int y = 0; y < 3; y++) 
			{
				small[counter] = new Particles();
				
				small[counter].X = (x * 100);
				small[counter].Y = (y * 100);
				
				small[counter].directionX = Math.abs(((x+1) * 100) - maxX/2);
				small[counter].directionY = Math.abs(((y+1) * 100) - maxY/2);
				
				small[counter].speed = 0.01f;
				
				smallFar[counter] = new Particles();
				
				smallFar[counter].X = (x * 130);
				smallFar[counter].Y = (y * 100);
				
				smallFar[counter].directionX = Math.abs(maxX/2 - (x+1) * 130);
				smallFar[counter].directionY = Math.abs(maxY/2 - (y+1) * 100);
				
				smallFar[counter].speed = 0.01f;
				
				counter++;
			}
		}
		
		counter = 0;
		for(int x = 0; x< 5; x++)
		{
			for(int y = 0; y<2; y++)
			{
				large[counter] = new Particles();
				
				large[counter].X = x * 100 ;
				large[counter].Y = y * 100 ;
				
				large[counter].directionX = Math.abs(maxX/2 - (x+1) * 100);
				large[counter].directionY = Math.abs(maxY/2 - (y+1) * 100);
				
				large[counter].speed = 0.005f;
				
				counter++;
			}
		}
		
	}
	
	public static void Update()
	{
		for (int i = 0; i < small.length; i++) 
		{
			small[i].X += small[i].directionX * small[i].speed;
			small[i].Y += small[i].directionY * small[i].speed;
			
			if (small[i].X > maxX || small[i].X < 0) small[i].directionX = -small[i].directionX;
			if (small[i].Y > maxY || small[i].Y < 0) small[i].directionY = -small[i].directionY;
			
			smallFar[i].X += smallFar[i].directionX * smallFar[i].speed;
			smallFar[i].Y += smallFar[i].directionY * smallFar[i].speed;
			
			if (smallFar[i].X > maxX || smallFar[i].X < 0) smallFar[i].directionX = -smallFar[i].directionX;
			if (smallFar[i].Y > maxY || smallFar[i].Y < 0) smallFar[i].directionY = -smallFar[i].directionY;
			
		}
		
		for (int i = 0; i < large.length; i++) 
		{
			large[i].X += large[i].directionX * large[i].speed;
			large[i].Y += large[i].directionY * large[i].speed;
			
			if (large[i].X > maxX || large[i].X < 0) large[i].directionX = -large[i].directionX;
			if (large[i].Y > maxY || large[i].Y < 0 ) large[i].directionY = -large[i].directionY;
		}
		
		
	}
	
	public static void Render(Graphics g)
	{
		for (int i = 0; i < small.length; i++) 
		{
			g.drawImage(smallTex, small[i].X, small[i].Y, null);
		}
		
		for (int i = 0; i < smallFar.length; i++) 
		{
			g.drawImage(smallFarTex, smallFar[i].X, smallFar[i].Y, null);
		}
		
		for (int i = 0; i < large.length; i++) 
		{
			g.drawImage(largeTex, large[i].X, large[i].Y, null);
		}
		
		//g.drawImage(smallTex, x++, x+=2, null);
		
	}
	

}
