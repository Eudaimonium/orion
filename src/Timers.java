
public class Timers 
{
	public static int targetFramerate = 60;
	
	private static float measured = 0, target;
	private static float waitMS = 0;
	
	public static void Start()
	{
		measured = (System.nanoTime() / 1000) / 1000;
		
		target = 1/(float)targetFramerate;
		target *= 1000;
	}
	
	public static void End()
	{
		measured = (System.nanoTime()/ 1000000) - measured;
		
		waitMS = target - measured;
		
		if (waitMS < 0) waitMS = 0;
		Wait();
		
	}
	
	private static void Wait()
	{
		try {
			Thread.sleep((long) waitMS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
