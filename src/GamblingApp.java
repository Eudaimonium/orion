import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class GamblingApp implements KeyListener {

	public JFrame frame;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GamblingApp window = new GamblingApp();
					window.frame.setLocation(0, 0);
					window.frame.setSize(1280, 800);
					
					window.frame.setUndecorated(true);
					window.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		//CODE
		GFX.Initialize();
	}

	/**
	 * Create the application.
	 */
	public GamblingApp() {
		initialize();
	}
	
	public static Timer timer;

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 1280, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		GFX panel = new GFX();
		panel.setBackground(new Color(0, 0, 102));
		panel.setBounds(0, 0, 1280, 800);
		frame.getContentPane().add(panel);
		
		panel.addNotify();
		panel.addKeyListener(this);
		
		panel.requestFocus();
		
		timer = new Timer(0, new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
				
				  frame.repaint();
				}
			});
		timer.setRepeats(true);
		
		timer.start();
	}

	@Override
	public void keyPressed(KeyEvent arg0) 
	{
		// TODO Auto-generated method stub
		if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			System.exit(0);
		}
	}	

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
